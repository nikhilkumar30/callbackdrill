/*
Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const getBoardsInfo = require("./callback1");
const getListInfo = require("./callback2");
const cardsInfo = require("./callback3");

function getAllData(boardData, listData, cardData) {
  const boardID = "mcu453ed";
  getBoardsInfo(boardID, boardData, (err, data) => {
    if (err) {
      console.error(`Error: ${err}`);
    } else {
      console.log("Board Information of Thanos boards :", data);
    }

    getListInfo(boardID, listData, (err, data) => {
      if (err) {
        console.error(`Error: ${err}`);
      } else {
        console.log("List Information :", data);
      }
      const listIDs = data.map((list) => list.id);
      console.log(listIDs)
      listIDs.forEach((listID) => {
        cardsInfo(listID, cardData, (err, data) => {
          if (err) {
            console.error(`No card for this id ${listID} ${err}`);
          } else {
            console.log(`card list of ${listID}`, data);
          }
        });
      });
    });
  });
}
module.exports = getAllData;
