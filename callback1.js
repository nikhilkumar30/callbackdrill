/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

function getBoardsInfo(boardID, boardData, callback) {
  const board = boardData.find((board) => board.id === boardID);

  if (board) {
    callback(null, board);
  } else {
    callback("Error", null);
  }
}

module.exports = getBoardsInfo;
