/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const getBoardsInfo = require("./callback1");
const getListInfo = require("./callback2");
const cardsInfo = require("./callback3");

function getAllData(boardData, listData, cardData) {
  const boardID = "mcu453ed";
  getBoardsInfo(boardID, boardData, (err, data) => {
    if (err) {
      console.error(`Error: ${err}`);
    } else {
      console.log("Board Information of Thanos boards :", data);
    }

    getListInfo(boardID, listData, (err, data) => {
      if (err) {
        console.error(`Error: ${err}`);
      } else {
        console.log("List Information :", data);
      }
      const listID = "qwsa221";
      const listID2= "jwkh245";
      cardsInfo(listID, cardData, (err, data) => {
        if (err) {
          console.error(`Error ${err}`);
        } else {
          console.log(`card list of ${listID}`, data);
        }
      });

      cardsInfo(listID2, cardData, (err, data) => {
        if (err) {
          console.error(`Error ${err}`);
        } else {
          console.log(`card list of ${listID2}`, data);
        }
      });
    });
  });
}
module.exports = getAllData;