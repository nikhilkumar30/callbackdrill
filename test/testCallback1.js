const getBoardsInfo = require('../callback1');

const boardData = require('../boards_1.json')


let boardID = 'mcu453ed';

getBoardsInfo(boardID,boardData,(err,data)=>{
    if (err) {
        console.error(`Error: ${err}`);
    } else {
        console.log("Board Information :", data);
    }
})