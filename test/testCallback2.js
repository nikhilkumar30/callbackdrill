const getListInfo = require("../callback2");

const listData = require("../lists_1.json");

let boardID = "mcu453ed";

getListInfo(boardID, listData, (err, data) => {
  if (err) {
    console.error(`Error: ${err}`);
  } else {
    console.log("List Information :", data);
  }
});
